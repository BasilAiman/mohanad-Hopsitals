<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect('livedata');
});

Route::get('logout', 'HomeController@logout');



Route::get('/livedata', 'HomeController@index');
Route::post('/livedata', 'HomeController@updateLiveData');
Route::get('/contact', 'HomeController@ContactData');
Route::post('/contact', 'HomeController@updateContactData');

Route::get('/sections', 'SectionController@index');
Route::get('/sections/create', 'SectionController@create');
Route::post('/sections/create', 'SectionController@store');
Route::get('/sections/show/{section}', 'SectionController@show');
Route::get('/sections/edit/{section}', 'SectionController@edit');
Route::post('/sections/edit/{section}', 'SectionController@update');
Route::get('/sections/delete/{section}', 'SectionController@delete');
Route::post('/sections/delete/{section}', 'SectionController@destroy');


Route::get("/users","UsersController@index");
Route::get("/users/show/{user}","UsersController@show");
Route::get("/users/delete/{user}","UsersController@delete");
Route::post("/users/delete/{user}","UsersController@destory");

Route::get("/users/createUser","UsersController@showRegistrationForm");
Route::post("/users/createUser","UsersController@create");



Auth::routes();

