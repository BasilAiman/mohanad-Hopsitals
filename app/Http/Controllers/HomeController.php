<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home/livedata');
    }

    /**
     * Update the live data for the logged in user
     */
    public function updateLiveData(Request $request){
        //Get post data
        $bodyContent = $request->getContent();
        $nurses = $request->nurses;
        $doctors = $request->doctors;
        $beds = $request->beds;
        //Update user
        $user = Auth::user();
        $user->nurses =  $nurses;
        $user->doctors = $doctors;
        $user->beds = $beds;
        $user->save();
        //Return the view
        return view('home/livedata');
    }


    public function ContactData()
    {
        return view('home/contact');
    }

    public function updateContactData(Request $request)
    {
        $bodyContent = $request->getContent();
        
        $contact_data = $request->contact_data;
        $map_link = $request->map_link;
        //Update user
        $user = Auth::user();
        $user->contact_data = $contact_data;
        $user->map_link = $map_link;        
        $user->save();
        //Return the view
        return view('home/contact');
    }




    
}
