<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\section;
class UsersController extends Controller
{
    public function index(){
        $users = User::all();
        return view("users/index",["users" => $users]);
    }
    public function show(User $user){
        $id = $user->id;
        $sections = section::where('user_id',$id)->get();
        
        return view("users/show",["user" => $user, "sections"=>$sections]);
    }
    public function delete(User $user){    
        $id = $user->id;
        $sections = section::where('user_id',$id)->get();                                     
        return view("users/delete",["user" => $user, "sections"=>$sections]);
    }
    public function destory(User $user){
        $id = $user->delete();
        return redirect("users/");
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
    /**
     * Show Registeration Form
     */
    public function showRegistrationForm(){
        return view("users/register");
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $data = $request->all();
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        return redirect("users/");
    }

    /**
     * Return the data from the mobile apps
     * @return \App\User
     */
    public function fetch_all(){
        $data = User::all();
        
        
        foreach($data as $user){
            $user['sections'] = $user->sections();
        }
        return response()->json($data);
    }
}