<?php

namespace App\Http\Controllers;

use App\section;
use Illuminate\Http\Request;
use Auth;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $sections = section::where('user_id',$user_id)->get();
        return view("sections/index",["sections" => $sections]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("sections/create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $section = new section();
        $section->fill($request->all());
        $section->user_id = Auth::user()->id;
        $section->save();
        return redirect("/sections");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\section  $section
     * @return \Illuminate\Http\Response
     */
    public function show(section $section)
    {        
        return view("sections/show",["section"=>$section]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(section $section)
    {
        return view("sections/edit",["section"=>$section]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, section $section)
    {
        $section->fill($request->all());
        $section->save();
        return redirect("/sections");
    }
    /**
     * show deleting page
     *
     * @param  \App\section  $section
     * @return \Illuminate\Http\Response
     */
    public function delete(section $section)
    {
        return view("sections/delete",["section"=>$section]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(section $section)
    {
        $section->delete();
        return redirect("/sections");
    }
}
