@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-title">
                    <span>Sections for {{Auth::user()->name}}</span>                    
                </div>
                <div class="ibox-content">
                        <a href="/sections/create" class="btn btn-primary pull-right">Add Section <i class="fa fa-plis"></i></a>
                    <table class="table table-responsive table-hover text-center">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>Tilte</td>
                                <td>Content</td>
                                <td></td>
                            </tr>
                        </thead>    
                        <tbody>
                            @foreach ($sections as $s)
                                <tr>
                                    <td>{{$s->id}}</td>
                                    <td>{{$s->title}}</td>
                                    <td>{{$s->content}}</td>
                                    <td>
                                        <a href="/sections/show/{{$s->id}}"><i class="fa fa-eye"></i></a>
                                        <a href="/sections/edit/{{$s->id}}"><i class="fa fa-edit"></i></a>
                                        <a href="/sections/delete/{{$s->id}}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table> 
                </div>
            </div>
           
        </div>                    
    </div>
</div>
@endsection
