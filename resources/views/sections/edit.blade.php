@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="ibox">
            <div class="ibox-title">
                <span>Edit section number {{$section->id}}</span>
            </div>
            <form method="POST" class="ibox-content">
                <div class="row">
                    @csrf
                    <div class="col-xs-12">                        
                        <div class="form-group">
                            <label for="">Title</label>
                            <input value={{$section->title}} name="title" type="text" class="form-control" placeholder="Title"/>
                        </div>
                        <div class="form-group">
                            <label for="">Content</label>
                            <textarea  name="content" placeholder="Content" id="" cols="30" rows="10" class="form-control"> {{ltrim($section->content)}}</textarea>
                        </div> 
                        <div class="form-group">                 
                            <button class="btn btn-primary pull-right m-r-lg" type="submit">
                                Submit
                            </button>
                            <a class="btn btn-default pull-right" href="/sections/">
                                Back
                            </a>
                        </div>                       
                    </div>
                </div>                
            </form>
        </div>
    </div>
</div>
@endsection
