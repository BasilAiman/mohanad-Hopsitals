@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="ibox">
            <div class="ibox-title">
                <span>Create a new section</span>
            </div>
            <form method="POST" class="ibox-content">
                <div class="row">
                    @csrf
                    <div class="col-xs-12">
                        <h2>Are you sure that you want to delete this record?</h2>
                    </div>
                    <div class="col-xs-12">                        
                        <div class="form-group">
                            <label for="">Title</label> <br/>
                            <span>{{$section->title}}</span>
                        </div>
                        <div class="form-group">
                            <label for="">Content</label> </br>
                            <span>{{$section->content}}</span>
                        </div> 
                        <div class="form-group">
                            <a class="btn btn-default pull-right m-r-lg" href="/sections/">
                                Back
                            </a>
                            <button class="btn btn-danger pull-right" type="submit">
                                Delete
                            </button>
                        </div>                       
                    </div>
                </div>                
            </form>
        </div>
    </div>
</div>
@endsection
