@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="ibox">
            <div class="ibox-title">
                <span> Details for {{ $user->name}}</span>
            </div>
            <form method="POST" class="ibox-content">
                <div class="row">
                    @csrf
                    <div class="col-xs-12">                        
                        <div class="form-group">
                            <label for="">Name</label> <br/>
                            <span>{{$user->name}}</span>
                        </div>
                        <div class="form-group">
                            <label for="">Beds</label> </br>
                            <span>{{$user->beds}}</span>
                        </div> 
                        <div class="form-group">
                            <label for="">Nurses</label> </br>
                            <span>{{$user->nurses}}</span>
                        </div> 
                        <div class="form-group">
                            <label for="">Doctors</label> </br>
                            <span>{{$user->doctors}}</span>
                        </div> 
                        <div class="form-group">
                            <label for="">Map Link</label> </br>
                            <span>{{$user->map_link}}</span>
                        </div> 
                        <div class="form-group">
                            <label for="">Contact data</label> </br>
                            <span>{{$user->contact_data}}</span>
                        </div> 
                        <div class="form-group">
                            <label for="">Sections</label> </br>
                            <table class="table table-responsive table-hover text-center">
                                <thead>
                                    <tr>
                                        <td>Title</td>
                                        <td>Content</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sections as $S)
                                        <tr>
                                            <td>{{$S->title}}</td>
                                            <td>{{$S->content}}</td>
                                        </tr>                                        
                                    @endforeach

                                </tbody>
                            </table>
                        </div>                         
                        <div class="form-group">
                            <a class="btn btn-primary pull-right m-r-lg" href="/users/">
                                Back
                            </a>
                        </div>                       
                    </div>
                </div>                
            </form>
        </div>
    </div>
</div>
@endsection
