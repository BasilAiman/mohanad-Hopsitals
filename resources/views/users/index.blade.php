@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xs-12">
            <div class="ibox">
                <div class="ibox-title">
                    <span>Users</span>                    
                </div>
                <div class="ibox-content">
                        <a href="/users/createUser" class="btn btn-primary pull-right">Add Hospital <i class="fa fa-plis"></i></a>
                    <table class="table table-responsive table-hover text-center">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>Name</td>
                                <td></td>
                            </tr>
                        </thead>    
                        <tbody>
                            @foreach ($users as $u)
                                <tr>
                                    <td>{{$u->id}}</td>
                                    <td>{{$u->name}}</td>
                                    <td>
                                        <a href="/users/show/{{$u->id}}"><i class="fa fa-eye"></i></a>
                                        <a href="/users/delete/{{$u->id}}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table> 
                </div>
            </div>
           
        </div>                    
    </div>
</div>
@endsection
