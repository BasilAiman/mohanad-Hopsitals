@inject('nav_helper', 'App\Navigation')


<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ Auth::user()->name }}</strong>
                            </span> 
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="#">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{{ $nav_helper::isActiveRoute('livedata') }}">
                <a href="{{ url('/livedata') }}"><i class="fa fa-heartbeat"></i> <span class="nav-label">Live Data </span></a>
            </li>
            <li class="{{ $nav_helper::isActiveRoute('sections') }}">
                <a href="{{ url('/sections') }}"><i class="fa fa-puzzle-piece"></i> <span class="nav-label">Sections</span> </a>
            </li>
            <li class="{{ $nav_helper::isActiveRoute('contact') }}">
                <a href="{{ url('/contact') }}"><i class="fa fa-envelope-open"></i> <span class="nav-label">Contact data</span> </a>
            </li>
            @if (Auth::user()->name == "admin")
                @include("layouts.adminNav")
            @endif
        </ul>

    </div>
</nav>
