@inject('nav_helper', 'App\Navigation')


<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">     
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">Please login to use the system</strong>
                            </span>                             
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    HP+
                </div>
            </li>       
            <li class="{{ $nav_helper::isActiveRoute('login') }}">
                <a href="{{ url('/login') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Login</span> </a>
            </li>
       
        </ul>

    </div>
</nav>
