@inject('nav_helper', 'App\Navigation')

<li class="{{ $nav_helper::isActiveRoute('users') }}">
    <a href="{{ url('/users') }}"><i class="fa fa-hospital-alt"></i> <span class="nav-label">All Hospitals</span> </a>
</li>


