@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xs-12">
            <form class="ibox" method="POST"  action="/contact">
                @csrf

                <div class="ibox-title">
                    <span>Contact data </span>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-gorup">
                                <label for="Contact info"></label>
                                <textarea name="contact_data" class="form-control" placeholder="Contact Info" name="" id="" cols="30" rows="10">{{Auth::user()->contact_data}}</textarea>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-gorup">
                                <label for="">Map Link</label>
                                <input name="map_link" value="{{Auth::user()->map_link}}" placeholder="Map link" type="text" class="form-control">
                             </div>
                        </div>             
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-primary pull-right m-r-lg" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
        
        
        
        
        
        
        
    </div>
</div>
@endsection
