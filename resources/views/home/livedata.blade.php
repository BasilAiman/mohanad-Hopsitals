@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xs-12">
            <form class="ibox" method="POST"  action="/livedata">
                @csrf

                <div class="ibox-title">
                    <span>Live Data</span>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <table class="table table-responsive table-hover text-center">
                            <thead>
                                <tr>
                                    <td><b>#</b></td>
                                    <td><b>Capabilites</b></td>
                                    <td><b>Count</b></td>
                                </tr>                            
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Doctors</td>
                                    <td>
                                        <input type="text"
                                            name="doctors"
                                            value="{{Auth::user()->doctors}}"
                                            class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Nurses</td>
                                    <td>
                                        <input type="text"
                                            name="nurses"
                                            value="{{Auth::user()->nurses}}"
                                            class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Beds</td>
                                    <td>
                                        <input type="text"
                                            name="beds"
                                            value="{{Auth::user()->beds}}"
                                            class="form-control" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="col-xs-12">
                            <button class="btn btn-primary pull-right m-l-lg" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
        
        
        
        
        
        
        
    </div>
</div>
@endsection
